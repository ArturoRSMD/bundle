package com.arturorsmd.bundle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button buttonEnter;
    EditText edtxtData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonEnter = findViewById(R.id.btn_enter);
        edtxtData = findViewById(R.id.edtxt_data);

        buttonEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = new Bundle();
                data.putString("dataKey", edtxtData.getText().toString());
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtras(data);
                startActivity(intent);

            }
        });
    }
}
