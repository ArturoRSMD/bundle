package com.arturorsmd.bundle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class CityActivity extends AppCompatActivity {

    TextView txtData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        txtData = findViewById(R.id.txt_data);

        Bundle bundle = getIntent().getExtras();
        String dataText = bundle.getString("dataKey", "No data found");
        txtData.setText(dataText);

    }
}
